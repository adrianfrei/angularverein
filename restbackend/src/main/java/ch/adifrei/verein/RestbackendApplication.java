package ch.adifrei.verein;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import javax.inject.Inject;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import ch.adifrei.verein.domain.Mitglied;
import ch.adifrei.verein.domain.Team;
import ch.adifrei.verein.repository.MitgliedRepository;
import ch.adifrei.verein.repository.TeamRepository;

@SpringBootApplication
public class RestbackendApplication {

	@Inject
	private MitgliedRepository mitgliedRepo;
	
	@Inject
	private TeamRepository teamRepo;
	
	public static void main(String[] args) {
		SpringApplication.run(RestbackendApplication.class, args);
	}

	@Bean
	CommandLineRunner getRunner() {
		return new CommandLineRunner() {
			
			@Override
			public void run(String... arg0) throws Exception {
				Team firstTeam = teamRepo.save(new Team("FirstTeam"));
				Team secondTeam = teamRepo.save(new Team("SecondTeam"));
				mitgliedRepo.save(new Mitglied("Adi", "Frei", firstTeam));
				mitgliedRepo.save(new Mitglied("Steve", "Jobs", firstTeam));
				mitgliedRepo.save(new Mitglied("Bill", "Gates", secondTeam));
			}
		};
	}

	
}
