package ch.adifrei.verein.repository;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import ch.adifrei.verein.domain.Mitglied;

@CrossOrigin(methods={GET,POST, PUT, DELETE })
@RepositoryRestController
public interface MitgliedRepository extends CrudRepository<Mitglied, Long>{

}
