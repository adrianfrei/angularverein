package ch.adifrei.verein.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import org.springframework.hateoas.core.Relation;

@Entity
@Relation
public class Mitglied extends DomainAbs {

	private String vorname;
	private String nachname;

	@ManyToOne(targetEntity=Team.class)
	private Team team;

	public Mitglied() {
	}

	public Mitglied(String vorname, String nachname) {
		super();
		this.vorname = vorname;
		this.nachname = nachname;
	}
	

	public Mitglied(String vorname, String nachname, Team team) {
		super();
		this.vorname = vorname;
		this.nachname = nachname;
		this.team = team;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

}
