package ch.adifrei.verein.domain;

import javax.persistence.Entity;

import org.springframework.hateoas.core.Relation;

@Entity
@Relation
public class Team extends DomainAbs {

	private String teamName;

	public Team() {
	}

	public Team(String teamName) {
		super();
		this.teamName = teamName;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
}
