import { Component, OnInit } from '@angular/core';
import { TeamService } from "../service/team.service";
import { Router } from "@angular/router";
import { Team } from "../types/team";

@Component( {
    selector: 'app-team-list',
    templateUrl: './team-list.component.html',
    providers: [TeamService]
} )
export class TeamListComponent implements OnInit {

    teams: Array<Team>;


    constructor( private teamService: TeamService, private router: Router ) {
    }

    onSelect( team: Team ) {
        let link = ['/team', JSON.stringify( team._links.self.href )];
        this.router.navigate( link )
    }
    createTeam() {
        let link = ['/team'];
        this.router.navigate( link )
    }
    ngOnInit() {
        this.teamService.getTeams().subscribe( p => this.teams = p, error => alert( error ) )
    }

}
