import { Injectable } from '@angular/core';
import { Http, Headers,Response } from "@angular/http";
import { Observable } from "rxjs/Rx";
import { Team } from "../types/team";

@Injectable()
export class TeamService {

    private headers = new Headers( { 'Content-Type': 'application/json' } );


    private url: string = "http://localhost:8080/teams";

    constructor( private http: Http ) {
    }
    getTeams(): Observable<Array<Team>> {
        return this.http.get( this.url ).map( r => r.json()._embedded['teams'] as Array<Team> );
    }

    getTeam( link: string ): Observable<Team> {
        return this.http.get( link ).map( r => r.json() as Team );
    }
    deleteTeam( t: Team ): Observable<Response> {
        return this.http.delete( t._links.self.href );
    }

    updateTeam( t: Team ): Observable<Team> {
        return this.http.put( t._links.self.href, JSON.stringify( t ), { headers: this.headers } ).map( r => r.json() as Team );
    }
    saveTeam( t: Team ): Observable<Team> {
        return this.http.post( this.url, JSON.stringify( t ), { headers: this.headers } ).map( r => r.json() as Team );
    }

}
