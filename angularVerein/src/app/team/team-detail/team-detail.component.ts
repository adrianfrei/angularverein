import { Component, OnInit } from '@angular/core';
import { Team } from "../types/team";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { TeamService } from "../service/team.service";

@Component( {
    selector: 'app-team-detail',
    templateUrl: './team-detail.component.html',
    providers: [TeamService]
} )
export class TeamDetailComponent implements OnInit {

    team: Team;

    constructor( private teamservice: TeamService, private route: ActivatedRoute, private router: Router ) {
    }

    deleteTeam( t: Team ) {
        this.teamservice.deleteTeam( t ).subscribe( r => this.router.navigate( ['/teams'] ) );
    }

    saveTeam( t: Team ) {
        this.teamservice.saveTeam( t ).subscribe( r => this.router.navigate( ['/teams'] ) );
    }

    updateTeam( t: Team ) {
        this.teamservice.updateTeam( t ).subscribe( r => this.router.navigate( ['/teams'] ) );
    }

    ngOnInit() {
        let hal;
        this.route.params.forEach(( p: Params ) => {
            hal = p['hal'];
        }
        )
        if ( hal ) {
            this.teamservice.getTeam( JSON.parse( hal ) ).subscribe( p => this.team = p );
        }
        else {
            this.team = new Team( "" );
        }
    }

}
