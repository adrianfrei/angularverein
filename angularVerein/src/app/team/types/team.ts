
import { Links } from "../../common/hal/types/links";
import { TeamLinks } from "./team-links";

export class Team {
    teamName: String;
    _links: TeamLinks;

    constructor( teamName: String ) {
        this.teamName = teamName;
    }
}
