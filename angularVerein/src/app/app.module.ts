import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { MitgliedListComponent } from "./mitglied/mitglied-list/mitglied-list.component";
import { MitgliedDetailComponent } from './mitglied/mitglied-detail/mitglied-detail.component';
import { TeamListComponent } from './team/team-list/team-list.component';
import { TeamDetailComponent } from './team/team-detail/team-detail.component';

@NgModule( {
    declarations: [
        AppComponent,
        MitgliedListComponent,
        MitgliedDetailComponent,
        TeamListComponent,
        TeamDetailComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot( [{ path: 'mitglieder', component: MitgliedListComponent },//
                               { path: '', redirectTo: '/mitglieder', pathMatch: 'full' }, //
                               { path: 'mitglied/:hal', component: MitgliedDetailComponent },//
                               { path: 'teams', component: TeamListComponent }, //
                               { path: 'team/:hal', component: TeamDetailComponent }, //
                               { path: 'team', component: TeamDetailComponent }, //
                               { path: 'mitglied', component: MitgliedDetailComponent }] )//
                               
    ],
    providers: [],
    bootstrap: [AppComponent]
} )
export class AppModule { }
