import { MitgliedLinks } from "./mitglied-link";

export class Mitglied {
    vorname: String;
    nachname: String;
    _links: MitgliedLinks;

    constructor( vorname: String, nachname: String ) {
        this.vorname = vorname;
        this.nachname = nachname;
    }
}
