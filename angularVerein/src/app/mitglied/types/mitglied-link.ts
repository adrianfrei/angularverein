
import { Links } from "../../common/hal/types/links";
import { Self } from "../../common/hal/types/self";

export class MitgliedLinks extends Links{
    
    team: Self;
}
