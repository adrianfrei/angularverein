import { Injectable } from '@angular/core';
import { Mitglied } from "../types/mitglied";
import { Http, Response, Headers } from "@angular/http";
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class MitgliedService {

    private headers = new Headers( { 'Content-Type': 'application/json' } );


    private url: string = "http://localhost:8080/mitglieds";

    constructor( private http: Http ) {
    }
    getMitglieder(): Observable<Array<Mitglied>> {
        return this.http.get( this.url ).map( r => r.json()._embedded['mitglieds'] as Array<Mitglied> );
    }
    deleteMitglied( m: Mitglied ): Observable<Response> {
        return this.http.delete( m._links.self.href );
    }
    getMitglied( link: string ): Observable<Mitglied> {
        return this.http.get( link ).map( r => r.json() as Mitglied );
    }
    updateMitglied( m: Mitglied ): Observable<Mitglied> {
        return this.http.put( m._links.self.href, JSON.stringify( m ), { headers: this.headers } ).map( r => r.json() as Mitglied );
    }
    saveMitglied( m: Mitglied ): Observable<Mitglied> {
        return this.http.post( this.url, JSON.stringify( m ), { headers: this.headers } ).map( r => r.json() as Mitglied );
    }
}
