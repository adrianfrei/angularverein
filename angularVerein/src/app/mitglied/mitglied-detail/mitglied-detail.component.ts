import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router'
import { Mitglied } from "../types/mitglied";
import { MitgliedService } from "../service/mitglied.service";
import { TeamService } from "../../team/service/team.service";
import { Team } from "../../team/types/team";

@Component( {
    selector: 'app-mitglied-detail',
    templateUrl: './mitglied-detail.component.html',
    providers: [MitgliedService, TeamService]
} )
export class MitgliedDetailComponent implements OnInit {

    teams: Array<Team>;

    selectedTeam: Team;

    mitglied: Mitglied;

    constructor( private mitgliedService: MitgliedService, private teamService: TeamService, private route: ActivatedRoute, private router: Router ) {
    }

    onChange( newValue ) {
        this.selectedTeam = newValue;
        this.mitglied._links.team.href = this.selectedTeam._links.self.href;
    }

    deleteMitglied( m: Mitglied ) {
        this.mitgliedService.deleteMitglied( m ).subscribe( r => this.router.navigate( ['/'] ) );
    }

    updateMitglied( m: Mitglied ) {
        this.mitgliedService.updateMitglied( m ).subscribe( r => this.router.navigate( ['/'] ) );
    }
    saveMitglied( m: Mitglied ) {
        this.mitgliedService.saveMitglied( m ).subscribe( r => this.router.navigate( ['/'] ) );
    }

    ngOnInit() {
        let hal;
        this.route.params.forEach(( p: Params ) => {
            hal = p['hal'];
        }
        )
        if ( hal ) {
            this.teamService.getTeams().subscribe( p => this.teams = p );
            this.mitgliedService.getMitglied( JSON.parse( hal ) ).subscribe( p => {
                this.mitglied = p;
                this.teamService.getTeam( this.mitglied._links.team.href ).subscribe( t => {
                    this.selectedTeam = this.teams.find( x => x._links.self.href === t._links.self.href );
                } );
            } );
        }
        else {
            this.mitglied = new Mitglied( "", "" );
        }
    }

}
