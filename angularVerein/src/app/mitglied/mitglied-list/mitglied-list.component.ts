import { Component, OnInit, Inject } from '@angular/core';
import { Mitglied } from "../types/mitglied";
import { Router } from "@angular/router";
import { MitgliedService } from "../service/mitglied.service";

@Component( {
    selector: 'mitglied-list',
    templateUrl: './mitglied-list.component.html',
    providers: [MitgliedService]
} )
export class MitgliedListComponent implements OnInit {

    selectedMitglied: Mitglied;

    mitglieder: Array<Mitglied> = [];

    constructor( private mitgliedService: MitgliedService, private router: Router ) {
    }

    onSelect( mitglied: Mitglied ) {
        let link = ['/mitglied', JSON.stringify(mitglied._links.self.href)];
        this.router.navigate(link)
    }
    
    createMitglied():void{
        let link = ['/mitglied'];
        this.router.navigate(link)
    }

    ngOnInit() {
        this.mitgliedService.getMitglieder().subscribe( p => this.mitglieder = p, error => alert( error ) )
    }

}
