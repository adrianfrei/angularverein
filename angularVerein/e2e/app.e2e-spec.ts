import { AngularVereinPage } from './app.po';

describe('angular-verein App', () => {
  let page: AngularVereinPage;

  beforeEach(() => {
    page = new AngularVereinPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
